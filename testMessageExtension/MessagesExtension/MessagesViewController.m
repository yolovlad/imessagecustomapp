//
//  MessagesViewController.m
//  MessagesExtension
//
//  Created by Vlad Yalovenko on 09/12/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//

#import "MessagesViewController.h"


@interface MessagesViewController ()
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;

@end

@implementation MessagesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Conversation Handling
-(void)didBecomeActiveWithConversation:(MSConversation *)conversation {
    // Called when the extension is about to move from the inactive to active state.
    // This will happen when the extension is about to present UI.
    
    // Use this method to configure the extension and restore previously stored state.
}

-(void)willResignActiveWithConversation:(MSConversation *)conversation {
    // Called when the extension is about to move from the active to inactive state.
    // This will happen when the user dissmises the extension, changes to a different
    // conversation or quits Messages.
    
    // Use this method to release shared resources, save user data, invalidate timers,
    // and store enough state information to restore your extension to its current state
    // in case it is terminated later.
}

- (void)didSelectMessage:(MSMessage *)message conversation:(MSConversation *)conversation
{
    NSLog(@"1");
    NSExtensionContext *myExtension=[self extensionContext];
    [myExtension openURL:[NSURL URLWithString:@"http://google.com"] completionHandler:nil];
}

-(void)didStartSendingMessage:(MSMessage *)message conversation:(MSConversation *)conversation {
    NSLog(@"2");

    // Called when the user taps the send button.
}

-(void)didCancelSendingMessage:(MSMessage *)message conversation:(MSConversation *)conversation {
    NSLog(@"3");

    // Called when the user deletes the message without sending it.
    
    // Use this to clean up state related to the deleted message.
}

-(void)willTransitionToPresentationStyle:(MSMessagesAppPresentationStyle)presentationStyle {
    NSLog(@"4");

    // Called before the extension transitions to a new presentation style.
    
    // Use this method to prepare for the change in presentation style.
}

-(void)didTransitionToPresentationStyle:(MSMessagesAppPresentationStyle)presentationStyle {
    NSLog(@"5");

    // Called after the extension transitions to a new presentation style.
    
    // Use this method to finalize any behaviors associated with the change in presentation style.
}

#pragma IBActions
- (IBAction)didClickSendButton:(id)sender
{
    MSConversation *conversation = [MSConversation new];
    MSMessage *message = [MSMessage new];
    MSMessageTemplateLayout *layout = [MSMessageTemplateLayout new];
    
    UIImage *image = [self createImage];
    [layout setImage:image];
    [layout setCaption:@"Stepper value"];
    [message setLayout:layout];
    [message setURL:[NSURL URLWithString:@"google.com"]];
    
    [conversation insertMessage:message completionHandler:^(NSError *error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }];
}

- (IBAction)didClickStepper:(id)sender
{
    [[self valueLabel] setText:[NSString stringWithFormat:@"%.0f", [[self stepper] value]]];
}

#pragma self methods
- (UIImage *)createImage
{
    UIView *background = [UIView new];
    background.frame = CGRectMake(0, 0, 300, 300);
    background.backgroundColor = [UIColor whiteColor];
    
    UILabel *label = [UILabel new];
    label.frame = CGRectMake(75, 75, 150, 150);
    label.font = [UIFont systemFontOfSize:65];
    label.backgroundColor = [UIColor redColor];
    label.textColor = [UIColor whiteColor];
    label.text = [NSString stringWithFormat:@"%.0f",[[self stepper] value]];
    label.textAlignment = NSTextAlignmentCenter;
    label.layer.cornerRadius = label.frame.size.width/2.0;
    label.clipsToBounds = YES;
    
    [background addSubview:label];
    [self.view addSubview:background];
    
    
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    [background removeFromSuperview];
    
    return img;
}

@end
