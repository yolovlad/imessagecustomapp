//
//  MessagesViewController.h
//  MessagesExtension
//
//  Created by Vlad Yalovenko on 09/12/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//

#import <Messages/Messages.h>

@interface MessagesViewController : MSMessagesAppViewController

@end
